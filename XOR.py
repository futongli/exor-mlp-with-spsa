import numpy as np
from components.MLPmodel import MLPmodel
from components.optimizers import SPSA

train_x = np.array([[0, 1, 1], [0, 0, 1]])
train_y = np.array([1, 0, 0]).reshape(1, 3)
test_x = np.array([[0], [0]])

test_y = np.array([1]).reshape(1, 1)

layers_dims = (2, 2, 1)

'''
annealing = int(input('Annealing or not? Please use level 1, 10, or 100   '))

try:
    if annealing != 1 or annealing != 10 or annealing != 100:
        raise NameError('Annealing level: ' + str(annealing))

except NameError:
    print('Input an unsolvalbe annealing level! Please use level 1, 10, or 100')
'''

# Compute cost of MLP & create the optimizer class
max_iter = 500
optimizer = SPSA(a=100.0, c=2.0, A=max_iter/10.0, alpha=1.0, gamma=0.167)

# create the linear model
MLP_model = MLPmodel(layers_dims=layers_dims, optimizer=optimizer, num_iterations=max_iter)
parameters = MLP_model.L_layer_model(train_x, train_y)
print(parameters)

pred_train = MLP_model.predict(train_x, train_y, parameters)
pred_test = MLP_model.predict(test_x, test_y, parameters)