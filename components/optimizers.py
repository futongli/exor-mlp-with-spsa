import numpy as np
from components.MLP import L_model_forward_array


class SPSA:
    """
    An optimizer class that implements Simultaneous Perturbation Stochastic Approximation (SPSA)
    """
    def __init__(self, a, c, A, alpha, gamma):
        # Initialize gain parameters and decay factors
        self.a = a
        self.c = c
        self.A = A
        self.alpha = alpha
        self.gamma = gamma

        self.min_vals = -4.0
        self.max_vals = 4.0

        # counters
        self.t = 0


    def step(self, print_cost, num_iter, current_estimate, X, Y):
        """
        :param current_estimate: This is the current estimate of the parameter vector
        :return: returns the updated estimate of the vector
        """
        # Print the cost every 1000 training example.
        AL = L_model_forward_array(X, current_estimate)
        loss = np.sum((Y - AL)**2) / Y.shape[1]
        if print_cost and num_iter % 100 == 0:
            print ("Loss after iteration %i: %f" %(num_iter, loss))

        # get the current values for gain sequences
        a_t = self.a / (self.t + 1 + self.A)**self.alpha
        c_t = self.c / (self.t + 1)**self.gamma

        deltas = []
        current_estimate_plus = []
        current_estimate_minus = []
        # measure the loss function at perturbations
        for l in range(len(current_estimate)):
            # get the random perturbation vector from bernoulli distribution
            # it has to be symmetric around zero
            # But normal distribution does not work which makes the perturbations close to zero
            # Also, uniform distribution should not be used since they are not around zero   
            delta = np.random.randint(0,2, current_estimate[l].shape) * 2 - 1
            current_estimate_plus_ = current_estimate[l] + delta * c_t
            current_estimate_minus_ = current_estimate[l] - delta * c_t

            deltas.append(delta)
            current_estimate_plus.append(current_estimate_plus_)
            current_estimate_minus.append(current_estimate_minus_)

        loss_plus = np.sum((Y - L_model_forward_array(X, current_estimate_plus))**2) / Y.shape[1]
        loss_minus = np.sum((Y - L_model_forward_array(X, current_estimate_minus))**2) / Y.shape[1]    

        # Update the estimate of the parameter
        for l in range(len(current_estimate)):
            
            # compute the estimate of the gradient
            g_t = (loss_plus - loss_minus) / (2.0 * deltas[l] * c_t)
            current_estimate[l] = current_estimate[l] - a_t * g_t
 
            # Ignore results that are outside the boundaries
            if (self.min_vals is not None) and (self.max_vals is not None):      
                current_estimate[l] = np.minimum ( current_estimate[l], self.max_vals )
                current_estimate[l] = np.maximum ( current_estimate[l], self.min_vals )
            
        # increment the counter
        self.t += 1

        return current_estimate

