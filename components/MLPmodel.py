import numpy as np
from components.optimizers import SPSA
from components.MLP import initialize_parameters_deep_array, L_model_forward_array


class MLPmodel:
    def __init__(self, layers_dims, optimizer, num_iterations=3000,reg_factor=0.0):
        self.layers_dims = layers_dims
        self.optimizer = optimizer
        self.reg_factor = reg_factor
        self.num_iterations = num_iterations

        # Parameters initialization
        self.parameters = initialize_parameters_deep_array(self.layers_dims)
        print(self.parameters)

    def L_layer_model(self, X, Y, print_cost=True, plot=False):
        '''
        # conut the number of parameters in NN
        N = 0
        for l in range(len(self.layers_dims)-1):
            N += (self.layers_dims[l] * self.layers_dims[l+1] + self.layers_dims[l+1])      
        '''
        for i in range(0, self.num_iterations):
            # Update the weight
            self.W = self.optimizer.step(print_cost, i, self.parameters, X, Y)

        return self.W


    def predict(self, X, y, parameters, annealing=1):
        
        m = X.shape[1]
        p = np.zeros((1,m))
        
        # Forward propagation        
        probas = L_model_forward_array(X, parameters, annealing)

        # convert probas to 0/1 predictions
        for i in range(0, probas.shape[1]):
            if probas[0,i] > 0.5:
                p[0,i] = 1
            else:
                p[0,i] = 0
        
        #print results
        print ("predictions: " + str(p))
        print ("true labels: " + str(y))
        print("Accuracy: "  + str(np.sum((p == y)/m)))
            
        return p
