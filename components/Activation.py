import numpy as np
from enum import Enum


def sigmoid(Z, annealing):

    A = 1/(1+np.exp(-Z / annealing))
    
    return A



def relu(Z):

    A = np.maximum(0,Z)   

    return A
