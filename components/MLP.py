import numpy as np

from components.Activation import sigmoid, relu
from enum import Enum


class AcvtivationFunc(Enum):
    sigmoid = 1
    relu = 2


def initialize_parameters_deep_array(layer_dims):
    np.random.seed()
    L = len(layer_dims)  # number of layers in the nn

    '''
    [W0][w1][b0][b1]
    # W := (the # of nodes of layer l) * (the # of nodes of layer l+1)
    # b := the # of nodes in layer l
    # the odd numbers of paramerters are the wights, and the even numbers of parameters are biases
    '''
    parameters = []
    for l in range(L-1):
        W = np.random.randn(layer_dims[l+1], layer_dims[l]) * 0.01
        b = np.zeros((layer_dims[l+1], 1))
        parameters.append(W)
        parameters.append(b)

    return parameters


def linear_forward(A, W, b):

    Z = np.dot(W, A) + b
    
    assert(Z.shape == (W.shape[0], A.shape[1]))
    
    return Z


def linear_activation_forward(A_prev, W, b, activation, annealing):
    
    if activation == AcvtivationFunc.sigmoid:
        Z = linear_forward(A_prev, W, b)
        A = sigmoid(Z, annealing)
    
    elif activation == AcvtivationFunc.relu:
        Z = linear_forward(A_prev, W, b)
        A = relu(Z)

    assert (A.shape == (W.shape[0], A_prev.shape[1]))

    return A


def L_model_forward_array(X, parameters, annealing=1):

    A = X
    L = len(parameters[0])  # 2 times of layers in the neural network
    
    # Implement [LINEAR -> RELU]*(L-1).
    for l in range(0, L-1, 2):
        A_prev = A 
        A = linear_activation_forward(A_prev, parameters[l], parameters[l+1], AcvtivationFunc.relu, annealing)

    # Implement LINEAR -> SIGMOID.
    A_prev = A
    AL = linear_activation_forward(A_prev, parameters[L], parameters[L+1], AcvtivationFunc.sigmoid, annealing)

    assert(AL.shape == (1, X.shape[1]))
            
    return AL

